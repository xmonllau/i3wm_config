#_añadir a .bashrc
#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi

# añadir bin al path
#if [ -d ~/bin ]; then
#	export PATH=$PATH:~/bin
#fi

#prompt de usuario
#PS1='\[\e[0;32m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;32m\]\$\[\e[m\]\[\e[1;37m\] '
#prompt de root
#PS1='\[\e[0;31m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[0;31m\]\$\[\e[m\]\[\e[0;32m\] '
########################################################################################

###_MIS_ALIAS

## ARCHLINUX
alias actualizar='sudo pacman -Syu && pamac update && yay -Syua --confirm'
# pamac (manjaro)
alias instalar_pamac='pamac install'
alias desinstalar_pamac='pamac remove'
alias actualizar_pamac='pamac update'
alias upgrade_pamac='pamac upgrade'
alias buscar_pamac='pamac search'
alias info_pamac='pamac info'
alias clean_pamac='pamac clean'
alias checkupdates_pamac='pamac checkupdates'
alias list_pamac='pamac list'
alias reinstalar_pamac='pamac reinstall'
alias clone_pamac='pamac clone'
alias build_pamac='pamac build'
# pacman
alias refresh-keys_pacman='sudo pacman-key --refresh-keys' #refresca les claus del packets.
alias init_pacman='pacman-key --init' #inicialitza les claus del packets.
alias populate_archlinux_pacman='pacman-key --populate archlinux' #inicialitza les claus del packets (segon pas).
alias instalar_pacman='sudo pacman -Syu' #instala un paquete.
alias desinstalar_pacman='sudo pacman -Rs' #desinstala un paquete.
alias actualizar_pacman='sudo pacman -Syyu' #actualiza los paquetes.
alias buscar_pacman='pacman -Ss' #busca un paquete.
alias info_pacman='pacman -Si' #presenta la informacion del paquete.
alias ficheros_pacman='pacman -Fl' #listado de ficheros del paquete
alias dep_pacman='pacman -Si' #lista las dependencias de un paquete.
alias buscarIns_pacman='pacman -Qs' #busca paquetes arch ya instalados.
alias buscarFP_pacman='pacman -Fo' # busqueda del paquete a que pertenece el fichero
alias huerfanos_pacman='pacman -Qdt' #listado de paquetes huerfanos
alias listaPI_pacman='pacman -Syyu - > /tmp/paquetes_pacman.txt' #crea un fichero con la lista de los paquetes instalados.
alias mirrors_pacman='sudo pacman-mirrors --country Spain,Germany,France,United_Kingdom,United_States && sudo pacman-mirrors -f 0 -m rank && sudo pacman -Syyu' #nueva lista ordenada de mirrors
# aur yay
alias instalar_aur='yay -S'
alias desinstalar_aur='yay -R'
alias actualizar_aur='yay -Syua --noconfirm'
alias buscar_aur='yay -Ss'
alias info_aur='yay -Si'
alias ficheros_aur='yay -F'
# pactree
alias dependencias='pactree -r' #dependencias del paquete
alias dependenciasT='pactree' #todas las dependencias del paquete
# paccache
alias vaciar_cache_pacman='sudo paccache -rk 1' #vacia la carpeta de la cache de los paquetes, excepto ultima version.


## SISTEMA
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias du.='du -sch ./'
alias CLEAR='tput reset'
alias cls='tput reset'
alias CLS='tput reset'
alias liberar_ram='sudo sync && sudo sysctl -w vm.drop_caches=3'
alias liberar_miniaturas='du -sh ~/.thumbnails && rm -rf ~/.cache/thumbnails/*'
alias dmesg_log='dmesg -H -T'
alias bd='. bd -si'
alias dirtotal='du -hs'
dir_ocupat() { du -hs $1; }
alias home_du='du -sh ~/'
alias disk_lliure='du -sch ./'
alias nautilus+='sudo nautilus'
alias gtk2='apt-cache rdepends libgtk2.0-0 > /tmp/GTK2.txt'
alias gtk3='apt-cache rdepends libgtk-3-0 > /tmp/GTK3.txt'
alias qt4='apt-cache rdepends libqtcore4 > /tmp/QT4.txt'
alias qt5='apt-cache rdepends libqtcore4 > /tmp/QT5.txt'
alias alias2pdf='text2pdf -A4 -s14 -c60 -t4 ~/.bash_aliases > /tmp/alias.pdf'
alias velocitat_xarxa='speedtest --simple'
duplicats() { fdupes -r -s -S $1 > /tmp/duplicats && joe /tmp/duplicats; }
alias INXI='inxi -Fxz'
alias traducir_texto='chromium --new-window -app http://www.wordreference.com/es/translation.asp?tranword="$(xclip -o)"'

#bash
alias bash_profile='joe ~/.bash_profile'
alias bash_history='joe ~/.bash_history'
alias bash_aliases='joe ~/.bash_aliases'
alias bash_logout='joe ~/.bash_logout'

#Observatori de l'Ebre
alias fotosolar='~/GIT/ObsEbre/LaFotoDelSol/LaFotoDelSol.py'

#descargas(wget)
alias dl-web='wget --header="Accept: text/html" --user-agent="Mozilla/5.0 (X11; Linux amd64; rv:32.0b4) Gecko/20140804164216 Manjaro I3WM Firefox/32.0b4" --referer=http://www.google.com -e robots=off -k -r https://file.wikileaks.org'

#screencast ffmpeg
screencast_ini() {
	notify-send 'Inicializando screencast $1 - $2 ...' &&
	ffmpeg -f alsa -i hw:0 -f x11grab -s $1 -r $2 -i :0.0 -acodec libmp3lame -async 1 -vcodec libx264 -preset ultrafast -crf 0 -threads 4 /tmp/screencast.mp4;
}
screencast_stop() {
	notify-send 'Finalizando screencast...' &&
	killall ffmpeg;
}
alias screencast_uhd='screencast_ini 10 3840x2160'
alias screencast_fhd='screencast_ini 10 1920x1080'
alias screencast_hd='screencast_ini 10 1280x720'
#alias screencast_stop="notify-send 'Finalizando screencast...' && killall ffmpeg"

#cd
alias tmp='cd /tmp'
alias bin='cd ~/bin'
alias mnt='cd /mnt'
alias dades='cd /mnt/dades'
alias dades+='cd /mnt/dades+'
alias Dropbox='cd /mnt/Dropbox/Dropbox'
alias links='cd /tmp/links'

# Dropbox
alias dropbox_start="echo fs.inotify.max_user_watches=100000 | sudo tee -a /etc/sysctl.conf; sudo sysctl -p && dropbox start"
alias dropbox_du='du -sh /mnt/Dropbox/Dropbox/'
alias dropbox_mc='mc ~/ /mnt/Dropbox/Dropbox'
alias dropbox_cd='cd /mnt/Dropbox/Dropbox'

# Midnight Commander
alias MC='mc -ba'
alias mc+='sudo mc'
alias MC+='sudo mc -ba'
alias mc_openclipart='mc ~/openclipart.org/usuarios ~/openclipart.org'

# Grabar un iso de windows (fichero.iso /dev/sdX)
alias 2usbW='woeusb -d'

# grub
alias actualizar_grub='sudo grub-mkconfig && sudo update-grub'

# free basic compiler
compilar_freebasic_qb() {
	fbc -lang qb $1;
}
alias FBC='fbc -lang qb'

# comandos SSH
alias ssh_keygen='ssh-keygen -t rsa'
alias ssh_copykey='ssh-copy-id -i ~/.ssh/id_rsa.pub'

# montar directorios de red
alias nas="sudo mount /mnt/nas && notify-send '/mnt/nas MONTADO'"

# root kits
alias rkHunter='sudo rkhunter --update && sudo rkhunter --check'


# Directorios cifrados

# ENCFS
alias encfs_dropbox_m='encfs ~/Dropbox/.encfs ~/encfs/Dropbox'
alias encfs_dropbox_u='fusermount -u ~/encfs/Dropbox'
alias encfs_XP_m='encfs /mnt/nas/.encfs/XP ~/encfs/XP'
alias encfs_XP_u='fusermount -u ~/encfs/XP'

# CRYFS
alias cryfs_dropbox_m='cryfs ~/Dropbox/.cryfs ~/cryfs/Dropbox'
alias cryfs_dropbox_u='fusermount -u ~/cryfs/Dropbox'
#alias cryfs_XP_m='encfs /mnt/nas/.cryfs/XP ~/encfs/XP'
#alias cryfs_XP_u='fusermount -u ~/cryfs/XP'


## DEBIAN
## apt
alias actualizar_apt='sudo apt update && sudo apt upgrade'
alias buscar_apt='apt search'
alias instalar_apt='sudo apt install'
alias desinstalar_apt='sudo apt purge'
alias add_ppa='sudo add-apt-repository ppa:'

## I3WM
alias i3_config='joe ~/.config/i3/config'
alias i3_status='joe ~/.config/i3/py3status'
alias cp_fondo_grub='sudo cp ~/.config/i3/fondo.png /usr/share/grub'

## EDITORES
alias geany+='sudo geany'
alias joe+='sudo joe'

## ordenadores_remotos
alias micasa='ssh -Y pi@192.168.1.9'
alias _micasa='ssh -Y pi@micasa.monllau.es'

## mp3
alias video2mp3='ConvertVideo.py -f mp3 -i'

## GIT
alias Ginit='git init' #iniciamos seguimiento de git.
alias Gname='git config --local user.name "Xavi Monllau"'
alias Gemail='git config --local user.email "xavi@monllau.cat"'
alias Gpush='git push' #sube y actualiza los cambios en el servidor.
alias Gpull='git pull' #descargamos y sincronizamos las actualizaciones.
alias Gstatus='git status' #estado actual.
alias Gadd='git add' #añadimos ficheros (-a)
alias Grm='git rm --cached' #borramos el seguimiento del fichero.
alias Gcommit='git commit -m' #guardamos cambios actuales.
alias Gcheckout='git checkout' #viajes en el tiempo.
alias Glog='git log' #visualizar cambios.
alias Greset='git reset' #soft, mixer, hard,
alias Gbranch='git branch' #creamos o borramos una rama.
alias Gmerge='git merge' #fusionamos la rama
alias Gsh='cls && git-sh' #una shell para git.
alias Gtk='gitk --all&' #un entorno grafico de git.
alias Gmaster='git checkout master' #viajamos a la rama master.
alias Gnew='git checkout new' #viajamos a la rama new.
alias Gtest='git checkout test' #viajamos a la rama test.
alias Gclone='git clone' #clonamos un repositorio.
alias Gmarlin='cd ~/GIT/clone/Marlin'
alias Glagrossa='cd ~/GIT/LaGrossa'
alias Gdu='du -sch .git' #¿cuanto ocupa el almacen .git?

## PYTHON
## pip
alias actualizar_pip='sudo pip install -U'
alias buscar_pip='pip search'
alias info_pip='pip show'
alias instalar_pip='sudo pip install'
alias desinstalar_pip='sudo pip uninstall'
## flaskweb
alias flaskweb_plantilla='cd ~/flaskweb/plantilla/ && ./run_flask.py d'
alias flaskweb_oeb='cd ~/flaskweb/ && sudo ./app.py'
alias flaskweb_micasa='cd ~flaskweb/micasa.monllau.es && ./run_flask.py d'
alias flaskweb_microblog='cd ~flaskweb/microblog-version-0.18 && ./run.py d'

## LAGROSSA
alias LaGrossa='LaGrossa.py --gui&'

##_openclipart
alias openclipart='dl-openclipart.py'
alias openclipart_u='dl-openclipart.py -u'
alias openclipart_tail='tail -f ~/openclipart.org/openclipart.log'
alias openclipart_joe='joe ~/openclipart.org/openclipart.log'
alias openclipart_mc='mc ~/openclipart.org/usuarios ~/openclipart.org'
alias openclipart_du='du -sh ~/openclipart.org/usuarios && du -sh ~/openclipart.org/usuarios.cleaner'
alias openclipart_pif='dl-openclipart.py --pif '

##_TimeLapse
alias foto2mp4='ffmpeg -loop 1 -y -t 10 -shortest video.mp4 -i'
alias tl_g6='TimeLapse.py --idi $1  -c 4608.2592.0.432'
alias tl_darktable='TimeLapse.py --idi /tmp/darktable --odi /tmp/darktable.tl --hd hd480'
alias tl_tmp='TimeLapse.py --idi ./ --ofi /tmp/tl_tmp -n --hd hd480 --range'
alias tl_links='TimeLapse.py --idi /tmp/links'
alias stopmotion='TimeLapse.py --idi ./ --odi /tmp/StopMotion --fps 15 --fr 15 --hd hd720 -a 16:9 --vext mp4 --ofi sm_'
alias tl_webcam='TimeLapse.py --idi /tmp/links --odi /tmp/webcam --ofi webcam --hd vga --fg 5 -n'
alias tl-ConvertBitmap='TimeLapse.py --idi /tmp/ConvertBitmap --odi /tmp/ConvertBitmap'

##_ConvertBitmap.py
alias 2indalo='ConvertBitmap.py --odi /tmp/Indalo/ --fmt pdf --dpi 300 --svg eps --cmp --ifi'
alias 2print24='ConvertBitmap.py --odi ~/tmp/Print24/ --fmt tif --dpi 300 --color CMYK --icc Fogra39L.icc -sng 2 --ifi'
alias 2pdf='ConvertBitmap.py -f pdf --ifi'
alias 2tif='ConvertBitmap.py -f tif --ifi'
alias 2png='ConvertBitmap.py -f png --ifi'

##_ConvertVideo
alias 2mp3='ConvertVideo.py --idi ./ -f mp3'
alias video2jpg='ffmpeg f%6d.jpg -i'

##_clasificar_media.py
alias fz50='clasificar_media.py -i ./ --cam fz50'
alias bw10='clasificar_media.py -i ./ --cam bw10'
alias g6='clasificar_media.py -i ./ --cam g6'
alias ex5000='clasificar_media.py -i ./ --cam ex5000'
alias bq='clasificar_media.py -i ./ --cam bq'
alias wileyfox='clasificar_media.py -i ./ --cam wileyfox'

##_BKP_RSYNC
alias dades_bkp_oeb='rsync -r -t -p -v --progress --delete --modify-window=1 -s /mnt/dades/ /mnt/nas/'
